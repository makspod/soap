<?php namespace App\Http\Controllers;

use App\Conversion;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$client = new
		\SoapClient(
				"http://www.webservicex.net/CurrencyConvertor.asmx?WSDL"
		);

		$result = $client->ConversionRate(['FromCurrency' => 'GBP', 'ToCurrency' => 'USD']);
		$conversion = Conversion::where(['from' => 'GBP', 'to' => 'USD'])->first();
		if(!$conversion) {
			$conversion = new Conversion();
		}
		$conversion->from = 'GBP';
		$conversion->to = 'USD';
		$conversion->result = $result->ConversionRateResult;
		$conversion->save();
		return 'Updated';
	}

}
